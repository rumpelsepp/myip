GO     ?= go

myipd:
	$(GO) build $(GOFLAGS) -o $@ .

update:
	$(GO) get -u
	$(GO) mod tidy

.PHONY: myip 
