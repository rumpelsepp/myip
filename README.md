# myip

`myip` is a small http service which returns the visitors public ip address and related rDNS in json.
Optionally, the geolocation-db.com service is queried and the result is included in the json as well.
In instance of this service is available at https://rumpelsepp.org/myip

## Example

```
$ curl -s https://rumpelsepp.org/myip | jq '.'
{
    "ip": "2003:cd:3721:3f00:fb43:5549:ab58:2a7a",
    "names": [
        "p200300CD37213F00FB435549AB582A7A.dip0.t-ipconnect.de."
    ],
    "geo_ip": {
        "country_code": "DE",
        "country_name": "Germany",
        "city": "Munich",
        "postal": "80796",
        "latitude": 48.15,
        "longitude": 11.5833
    }
}
```

## Deployment

Just put it behind `nginx` or similar and your ready to go.
Check the command line flags help `-h` for further options.
In an `nginx` `server {…}` block you might use this snippet:

```
location /myip {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://127.0.0.1:8000;
}
```
