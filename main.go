package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"time"

	"git.sr.ht/~rumpelsepp/helpers"
	"git.sr.ht/~sircmpwn/getopt"
	"github.com/Fraunhofer-AISEC/penlog"
)

var logger = penlog.NewLogger("", os.Stderr)

type server struct {
	reverseProxy         bool
	getGeoLocation       bool
	geoLocationApiServer string
	httpClient           *http.Client
}

type apiResponse struct {
	IP          string   `json:"ip,omitempty"`
	Names       []string `json:"names,omitempty"`
	GeoLocation struct {
		CountryCode string  `json:"country_code,omitempty"`
		CountryName string  `json:"country_name,omitempty"`
		City        string  `json:"city,omitempty"`
		Postal      string  `json:"postal,omitempty"`
		Latitude    float64 `json:"latitude,omitempty"`
		Longitude   float64 `json:"longitude,omitempty"`
		IPv4        string  `json:"-"`
	} `json:"geo_ip,omitempty"`
}

func (s *server) fetchGeoLocation(addr string, resp *apiResponse) error {
	url := fmt.Sprintf("%s/%s", s.geoLocationApiServer, addr)
	r, err := s.httpClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(body, &resp.GeoLocation); err != nil {
		return err
	}

	return nil
}

func (s *server) getRemoteAddress(req *http.Request) (string, error) {
	var (
		addr string
		err  error
	)
	if s.reverseProxy {
		addr = req.Header.Get("X-Forwarded-For")
		if addr == "" {
			return "", fmt.Errorf("reverse proxy is shit")
		}
	} else {
		addr, _, err = net.SplitHostPort(req.RemoteAddr)
		if err != nil {
			return "", err
		}
	}
	return addr, nil
}

func (s *server) handleRequest(w http.ResponseWriter, req *http.Request) {
	addr, err := s.getRemoteAddress(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := apiResponse{}

	if s.getGeoLocation {
		// The API server returns strange json in case of
		// an error. Just leave out the geolocation info in
		// this case.
		if err := s.fetchGeoLocation(addr, &resp); err != nil {
			logger.LogWarning(err)
			resp = apiResponse{}
		}
	}

	resp.IP = addr

	if names, err := net.LookupAddr(addr); err == nil {
		resp.Names = names
	}

	helpers.SendJSON(w, resp)
}

type runtimeOptions struct {
	reverseProxy   bool
	listen         string
	getGeoLocation bool
	help           bool
}

func main() {
	opts := runtimeOptions{}
	getopt.BoolVar(&opts.reverseProxy, "r", false, "A reverse proxy is used")
	getopt.StringVar(&opts.listen, "l", "127.0.0.1:8000", "Listen on this address:port")
	getopt.BoolVar(&opts.getGeoLocation, "g", false, "Query geo location information")
	getopt.BoolVar(&opts.help, "h", false, "Show help page and exit")

	err := getopt.Parse()
	if err != nil {
		panic(err)
	}

	if opts.help {
		getopt.Usage()
		os.Exit(0)
	}

	handler := server{
		reverseProxy:         opts.reverseProxy,
		getGeoLocation:       opts.getGeoLocation,
		geoLocationApiServer: "https://geolocation-db.com/json",
		httpClient: &http.Client{
			Timeout: 10 * time.Second,
		},
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", handler.handleRequest)

	httpServer := &http.Server{
		Addr:           opts.listen,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	logger.LogCritical(httpServer.ListenAndServe())
}
