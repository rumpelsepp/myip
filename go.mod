module git.sr.ht/~rumpelsepp/myip

go 1.13

require (
	git.sr.ht/~rumpelsepp/helpers v0.0.0-20200306122308-771c4d7a22b7
	git.sr.ht/~sircmpwn/getopt v0.0.0-20190808004552-daaf1274538b
	github.com/Fraunhofer-AISEC/penlog v0.1.2-0.20200624142937-c5bc9405e8ab
)
